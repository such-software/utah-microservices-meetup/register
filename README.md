# register

Code for [https://www.meetup.com/utah-microservices/events/291771641/](https://www.meetup.com/utah-microservices/events/291771641/).

The domain example isn't brilliant, but it shows off the OCC capabilities of [Message DB](https://github.com/message-db/message-db).

To get it running, you'll need Message DB running.

Modify `settings/message_store_postgres.json` to match your instance of Message DB.  Initially, you'll need to copy `settings/message_store_postgres.json.example`.

Then install the gems: `POSTURE=development ./install-gems.sh`

Finally, `ruby test/interactive/register.rb <some email address`.

License: Don't directly copy this into production because there is no warranty.  If you accept that, do what you'd like with the code.
