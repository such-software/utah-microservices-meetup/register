module Register
  module Controls
    module Events
      module Registered
        def self.example(email: nil)
          registered = Messages::Events::Registered.build

          registered.email = email || Email.example

          registered
        end
      end
    end
  end
end
