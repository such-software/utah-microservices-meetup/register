module Register
  module Messages
    module Events
      class Registered
        include Messaging::Message

        attribute :email, String
      end
    end
  end
end
