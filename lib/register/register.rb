module Register
  class Register
    include Dependency
    include Messaging::StreamName

    include Messages::Events

    dependency :write, Messaging::Postgres::Write

    category :identity

    def self.call(email)
      instance = build

      instance.(email)
    end

    def self.build
      new.tap do |instance|
        Messaging::Postgres::Write.configure(instance)
      end
    end

    def call(email)
      registered = Registered.build

      # If you are concerned about right-to-be-forgotten legislation, then you'd
      # probably want to hash this email before using it in the stream_name.
      registered.email = email

      stream_name = stream_name(email)

      write.initial(registered, stream_name)
    end
  end
end


