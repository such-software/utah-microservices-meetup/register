require_relative "./interactive_init"

if ARGV.length == 0
  puts "USAGE: ruby test/interactive/register.rb <an email address>"

  exit
end

email = ARGV[0]

begin
  Register::Register.(email)
  puts "Successfully registered #{email}"
rescue MessageStore::ExpectedVersion::Error => e
  puts <<-errormessage
    That email was already taken, so redirect to a screen that shows the user
    and prompt them to try again with a new one.
  errormessage
end
