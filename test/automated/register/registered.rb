require_relative "../automated_init"

context "Register" do
  context "Registered" do
    register = Register::Register.new

    email = Controls::Email.example

    email or fail

    register.(email)

    writer = register.write

    registered = writer.one_message do |event|
      event.instance_of?(Messages::Events::Registered)
    end

    test "Registered event is written" do
      refute(registered.nil?)
    end

    test "Registered is written to an identity stream" do
      written_to_stream = writer.written?(registered) do |stream_name|
        stream_name == "identity-#{email}"
      end

      assert(written_to_stream)
    end

    context "attributes" do
      test "email" do
        assert(registered.email == email)
      end
    end
  end
end
