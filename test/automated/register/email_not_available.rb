require_relative "../automated_init"

context "Register" do
  context "Email not available" do
    email = Controls::Email.example

    registered = Controls::Events::Registered.example(email:)
    registered.email == email or fail

    stream_name = "identity-#{email}"

    begin
       Messaging::Postgres::Write.(registered, stream_name, expected_version: :no_stream)
    rescue MessageStore::ExpectedVersion::Error => e
    end

    test "Registered event is not written" do
      assert_raises(MessageStore::ExpectedVersion::Error) do
        Register::Register.(email)
      end
    end
  end
end
